# Multi-Task
Multi Task and Multi Domain Learning Experiments with Neural Networks

Read more from here https://gananath.github.io/multi_task.html

# Requirments
- Python 2
- Keras
- TensorFlow
- scikit-image
- Matplotlib
- Numpy
- Pandas

# Citation
```
@misc{gananath2018,
  author = {Gananath, R.},
  title = {Multi-Task},
  year = {2018},
  publisher = {GitHub},
  journal = {GitHub repository},
  howpublished = {\url{https://github.com/Gananath/Multi-Task}}
}
```
**Current Best after 3000 epochs of training**
![current_best](https://github.com/Gananath/gananath.github.io/blob/master/images/new_multi_pred.jpg)
